package messagesearchplugin.actions;

 
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import javax.swing.plaf.TextUI;

import messagesearchplugin.Activator;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceFilterDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.content.IContentType;
import org.eclipse.core.runtime.content.IContentTypeManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.search.internal.ui.text.FileSearchPage;
import org.eclipse.search.ui.ISearchResultPage;
import org.eclipse.search.ui.ISearchResultViewPart;
import org.eclipse.search.ui.NewSearchUI;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorMapping;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkingSet;
import org.eclipse.ui.IWorkingSetManager;
import org.eclipse.ui.PlatformUI;

 
 
public class EditerPopupConvert implements IObjectActionDelegate {
	private Shell shell;
	private int totalCount;
	private int index;
	IPreferenceStore store = Activator.getDefault().getPreferenceStore();
	ISourceViewer viewer;
	ITextSelection textselect;
	String selectedText;
 
	public EditerPopupConvert() {
		super();
	}
 
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		shell = targetPart.getSite().getShell();
	}
 
 
	public void run(IAction action) {
		try {
			//get editor
			IEditorPart editorPart = Activator.getDefault().getWorkbench()
					.getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor();
				
			IEditorSite iEditorSite = editorPart.getEditorSite();
			viewer =  (ISourceViewer)editorPart.getAdapter(ITextOperationTarget.class);
			
			boolean isExist = false;

			//get selection provider
			ISelectionProvider selectionProvider = iEditorSite.getSelectionProvider();

			ISelection iSelection = selectionProvider.getSelection();	

			textselect = ((ITextSelection) iSelection);
			selectedText = textselect.getText();
			
			if(selectedText.length() == 0){
				MessageDialog.openInformation(
						shell,
						"MessageSearchPlugin",
						"다국어 처리할 텍스트를 드래그해 선택해 주십시오.");
				return;
			}
			
			IWorkspace workspace= ResourcesPlugin.getWorkspace(); 
			IProject[] projects = workspace.getRoot().getProjects();
			totalCount = projects.length;
			
			//진행바 표시
			Job job = new Job("test") {
	  			@Override
	  			protected IStatus run(IProgressMonitor monitor) {
	  				monitor.beginTask("start task", totalCount);
 
		  				//time consuming work here
	  				doExpensiveWork(monitor);
	  						
	  				//syncWithUI();
 
		  				return Status.OK_STATUS;
		  			}
 
		  		};
	  		job.setUser(true);
	  		job.schedule();  	
		  		
	  		//ikep만을 위한 코딩 - 아래 파일에서 먼저 찾아처리.
	  		/*
		  	try {
		  		
		  		IEditorInput input = (IEditorInput)editorPart.getEditorInput() ;
				IFile handlerChainFile = (IFile)input.getAdapter(IFile.class);
				
				String localUrl = handlerChainFile.getLocation().toString();
				
				localUrl = localUrl.substring(0, localUrl.indexOf("ikep4")+5);
				
				String uiUrl = localUrl+"/ikep4-portal/src/main/resources/i18n/ui-button_ko.properties";
				String msgUrl = localUrl+"/ikep4-portal/src/main/resources/i18n/message-common_ko.properties";
				
				boolean isPre = false;
				
				isPre = getPrepertyVal(uiUrl);
				
				if(!isPre){
					isPre = getPrepertyVal(msgUrl);
				}
		  		
				if(isPre){
					return;
				}
		  	} catch(Exception e){
		  		//e.printStackTrace();
		  	}
		  	*/
	  		
	  		//열린 프로젝트 전체 검사
			root:for(IProject project : projects ){
				
				index++;
				
				if(!project.isOpen()){continue;}
				
				String limitProject = store.getString("project").trim();
				
				//프로젝트 설정 있으면 그 프로젝트에서만 가져오기
				if(limitProject != null && !limitProject.equals("")){
					
					String[] limitProjectes = limitProject.split("[,]"); 
					
					if(limitProjectes.length > 0){
						ArrayList<String> mNewList = new ArrayList<String>(Arrays.asList(limitProjectes));
						
						if(!mNewList.contains(project.getName())){
							continue;
						}
					}
				}
				
				
				IResource[] members = project.members();
				
				for(IResource member : members ){
					
					if(member.getName().equals("src")){
						
						List<IFile> properties = new ArrayList<IFile>();
						getFiles(member, properties);
						
						for(IFile file :properties){
							
							boolean isText = getPrepertyVal(file.getLocation().toString());
							
							if(isText) {
								isExist = true;
								break root;
							}
							
						}
					}
					
				}
			}
			
			if(!isExist){
				MessageDialog.openInformation(
						shell,
						"MessageSearchPlugin",
						"해당 메세지 키가 없습니다.\n추가해야 할것 같습니다.");
			}
		
			//HandlerChain.action(handlerChainFile, shell, selectedText);
			
 
		} catch (Exception e) {		
			e.printStackTrace();
		}
	}
 
 
	public void selectionChanged(IAction action, ISelection selection) {
	}
	
	private void getFiles(IResource member, List<IFile> properties) throws CoreException{
		
		IFolder folder= (IFolder)member;
		
		for(IResource file : folder.members() ){
			 if (file.getType() ==  IResource.FILE) {
				 
				 if(file.getFileExtension() !=null && file.getFileExtension().equals("properties")){
					 IFile f = (IFile) file;
					 properties.add(f);
				 }
		           
		     } else{
		    	 getFiles(file, properties);
		     }
		}
		 
	}
	
	private void doExpensiveWork(IProgressMonitor monitor) {
		monitor.worked(index);
	}
 
	private void syncWithUI() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				MessageDialog.openInformation(shell, "message",
						"completed!");
			}
		});
	}
	
	/**
	 * 프로퍼티에서 해당 단어에 대한 키 가져오기.
	 * @param location
	 * @param selectedText
	 * @param textselect
	 * @return
	 * @throws IOException 
	 * @throws BadLocationException 
	 */
	private boolean getPrepertyVal(String location) throws IOException, BadLocationException{
		
		boolean isExist = false;
		
		FileInputStream fis = new FileInputStream(location);
		
		String tagText = store.getString("tag").trim();
		
		try {
			PropertyResourceBundle rr = new PropertyResourceBundle(fis);
			
			Enumeration <String> keys = rr.getKeys();
			
			while (keys.hasMoreElements()) {
				
				String key = keys.nextElement();
				String value = rr.getString(key);
				
				if(value.replaceAll(" ","").equals(selectedText.replaceAll(" ",""))){
					
					isExist = true;
							
					viewer.getDocument().replace(textselect.getOffset(), textselect.getLength(), tagText.replace("{key}", key).replace("{text}", value)); 
					
					index = totalCount;
					break;
				}
				
			}
			
		} finally {
		  fis.close();
		}
		
		return isExist;
		
	}
 
}