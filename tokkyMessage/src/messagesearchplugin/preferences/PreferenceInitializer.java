package messagesearchplugin.preferences;

import java.util.ArrayList;
import java.util.List;

import messagesearchplugin.Activator;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;


public class PreferenceInitializer extends AbstractPreferenceInitializer {
	
	
	List<String> keyList = new ArrayList<String>();
	IPreferenceStore store = Activator.getDefault().getPreferenceStore();
	
	@Override
	public void initializeDefaultPreferences() {
		
		setDefault("tag", "<spring:message code=\"{key}\" text=\"{text}\"/>");
		
	}
	
	
	private void setDefault(String key, String value){
		
		store.setDefault(key, value);
		keyList.add(key);
	}
}
