package messagesearchplugin.preferences;

import messagesearchplugin.Activator;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;


public class PreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage  {
	
	
	public PreferencePage() {
        super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("tag는 {key} : 프로퍼티 키, {text} : 텍스트 치환된다. \nproject는 해당 프로젝트 이름을 넣으면 그 프로젝트에서만 검색된다.");
	 }
	
	 public void createFieldEditors() {
		 
		 Group topGroup = new Group(getFieldEditorParent(), SWT.NULL);
         
		 //경로
          addField(new StringFieldEditor("tag", "tag : ", topGroup));
          addField(new StringFieldEditor("project", "project (구분자 ,) : ", topGroup));
          
          doLayoutAndData(topGroup,2, 300);
          
	 }
	 
	 public void init(IWorkbench workbench) {}
	 
	 private void doLayoutAndData(Group group, int numColumns, int widthHint) {
			GridLayout gl = new GridLayout(numColumns, false);
			group.setLayout(gl);
			GridData gd = new GridData(GridData.FILL_HORIZONTAL);
			gd.horizontalSpan = 1;
			gd.widthHint = widthHint;
			group.setLayoutData(gd);
		}

	 
	 @Override
	 public void performApply() {
		 
		 super.performApply();
		 
		 initialize();
        
     }
	 
	 @Override
	 public boolean performOk() {
		 
		 super.performOk();
        
		 return true;
     }
	 
	 


}
		
